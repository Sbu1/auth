﻿using AuthSystem.Areas.Identity.Data;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AuthSystem.Models
{
    public class AssignmentApplication : IValidatableObject
    {
        [Key]
        public int AssignmentId { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        [Required]
        [Display(Name = "Institution")]
        public string Institution { get; set; }
        [Column(TypeName = "nvarchar(100)")]
        [Display(Name = "Course")]
        public string Course { get; set; }
        [Column(TypeName = "tinyint")]
        [Display(Name = "Year")]
        [Range(1, 4)]
        public int Year { get; set; }
        [Column(TypeName = "datetime")]
        [Display(Name = "Due Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DueDate { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        [Display(Name = "Additional Assignment Notes")]
        public string AdditionalNotes { get; set; }

        [Column(TypeName = "nvarchar(255)")]
        [Display(Name = "Additional Assignment Notes")]
        public string FileName { get; set; }

        [Column(TypeName = "nvarchar(10)")]
        [Display(Name = "Application Status")]
        public string Status { get; set; }

        [Column(TypeName = "decimal(10,2)")]
        [Display(Name = "Due Amount")]
        public double Amount { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            if (DueDate <= DateTime.Now)
            {
                yield return new ValidationResult("Due date has passed already. If you need your assignment to be done asap. Please add that in notes");
            }
        }
    }
}
