﻿using AuthSystem.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthSystem.Models
{
  public class AssignmentContext : IdentityDbContext<ApplicationUser>
  {

    public AssignmentContext(DbContextOptions<AssignmentContext> options) : base(options)
    {

    }

    public DbSet<AssignmentApplication> AssignmentApplications { get; set; }
  }
}
