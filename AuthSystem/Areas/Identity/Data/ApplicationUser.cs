﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using AuthSystem.Models;
using Microsoft.AspNetCore.Identity;

namespace AuthSystem.Areas.Identity.Data
{
  // Add profile data for application users by adding properties to the ApplicationUser class
  public class ApplicationUser : IdentityUser
  {    
    [PersonalData]
    [Required]
    [Column(TypeName = "nvarchar(100)")]
    public string FirstName { get; set; }
    
    [PersonalData]
    [Required]
    [Column(TypeName = "nvarchar(100)")]
    public string LastName { get; set; }
    
    [PersonalData]
    [Required]
    [Column(TypeName = "date")]
    public DateTime DOB { get; set; }

    [PersonalData]
    [Required]
    [Column(TypeName = "nvarchar(100)")]
    [Display(Name = "Nationality")]
    public string Nationality { get; set; }

    public virtual ICollection<AssignmentApplication> AssignmentApplications { get; set; }
  }
}
