﻿using System;
using AuthSystem.Areas.Identity.Data;
using AuthSystem.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(AuthSystem.Areas.Identity.IdentityHostingStartup))]
namespace AuthSystem.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
              services.AddDbContext<AssignmentContext>(options =>
                  options.UseSqlServer(
                      context.Configuration.GetConnectionString("AuthSystemContextConnectionDev")));

              services.AddDefaultIdentity<ApplicationUser>(options =>
              {
                options.SignIn.RequireConfirmedAccount = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
              })
                    .AddEntityFrameworkStores<AssignmentContext>();
            });
        }
    }
}