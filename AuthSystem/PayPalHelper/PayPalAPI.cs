﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PayPalCheckoutSdk.Core;
using PayPalCheckoutSdk.Orders;
using PayPalHttp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentManagement.PayPalHelper
{
    public class PayPalAPI
    {
        public IConfiguration configuration { get; }

        public PayPalAPI(IConfiguration _config)
        {
            configuration = _config;
        }

        public async Task<string> getRedirectURLToPayPal()
        {
            try
            {
                return Task.Run(async () =>
                {
                    HttpResponse response = await CreatedPaypalPaymentAsync();
                    var result = response.Result<Order>();
                    return result.Links.First(x => x.Rel == "approve").Href;
                }).Result;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex, "Failed to login to PayPal");
                return null;
            }
        }

        //public async Task<PayPalPaymentExecutedResponse> executePayment(string paymentId, string playerId)
        //{
        //    try
        //    {
        //        HttpClient http = GetPaypalHttpClient();
        //        PayPalAccessToken accessToken = await GetPayPalAccessTokenAsync(http);
        //        return await ExecutePaypalPaymentAsync(http, accessToken, paymentId, playerId);
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex, "Failed to login to PayPal");
        //        return null;
        //    }
        //}


        //private HttpClient GetPaypalHttpClient()
        //{
        //    string sandbox = configuration["PayPal:urlAPI"];

        //    var http = new HttpClient
        //    {
        //        BaseAddress = new Uri(sandbox),
        //        Timeout = TimeSpan.FromSeconds(30),
        //    };

        //    return http;
        //}

        //private async Task<PayPalAccessToken> GetPayPalAccessTokenAsync(HttpClient http)
        //{
        //    byte[] bytes = Encoding.GetEncoding("iso-8859-1").GetBytes($"{configuration["PayPal:clientId"]}:{configuration["PayPal:secret"]}");

        //    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "/v1/oauth2/token");
        //    request.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(bytes));

        //    var form = new Dictionary<string, string>
        //    {
        //        ["grant_type"] = "client_credentials"
        //    };

        //    request.Content = new FormUrlEncodedContent(form);

        //    HttpResponseMessage response = await http.SendAsync(request);

        //    string content = await response.Content.ReadAsStringAsync();
        //    PayPalAccessToken accessToken = JsonConvert.DeserializeObject<PayPalAccessToken>(content);
        //    return accessToken;
        //}

        private async Task<HttpResponse> CreatedPaypalPaymentAsync()
        {
            var request = new OrdersCreateRequest();
            request.Prefer("return=representation");
            request.RequestBody(BuildRequestBody());
            
            var response = await client().Execute(request);
            return response;
        }

        private OrderRequest BuildRequestBody()
        {
            OrderRequest orderRequest = new OrderRequest()
            {
                //CheckoutPaymentIntent = "CAPTURE", first capture then auth as below, try to auth straight and see
                CheckoutPaymentIntent = "AUTHORIZE",
                ApplicationContext = new ApplicationContext
                {
                    BrandName = "EXAMPLE INC",
                    LandingPage = "BILLING",
                    UserAction = "CONTINUE",
                    ShippingPreference = "SET_PROVIDED_ADDRESS"
                },
                PurchaseUnits = new List<PurchaseUnitRequest>
                {
                 new PurchaseUnitRequest{
                    ReferenceId =  "PUHF",
                    Description = "Sporting Goods",
                    CustomId = "CUST-HighFashions",
                    SoftDescriptor = "HighFashions",
                    AmountWithBreakdown = new AmountWithBreakdown
                    {
                      CurrencyCode = "USD",
                      Value = "230.00",
                      AmountBreakdown = new AmountBreakdown
                      {
                        ItemTotal = new Money
                        {
                          CurrencyCode = "USD",
                          Value = "180.00"
                        },
                        Shipping = new Money
                        {
                          CurrencyCode = "USD",
                          Value = "30.00"
                        },
                        Handling = new Money
                        {
                          CurrencyCode = "USD",
                          Value = "10.00"
                        },
                        TaxTotal = new Money
                        {
                          CurrencyCode = "USD",
                          Value = "20.00"
                        },
                        ShippingDiscount = new Money
                        {
                          CurrencyCode = "USD",
                          Value = "10.00"
                        }
                      }
                    },
                    Items = new List<Item>
                    {
                      new Item
                      {
                        Name = "T-shirt",
                        Description = "Green XL",
                        Sku = "sku01",
                        UnitAmount = new Money
                        {
                          CurrencyCode = "USD",
                          Value = "90.00"
                        },
                        Tax = new Money
                        {
                          CurrencyCode = "USD",
                          Value = "10.00"
                        },
                        Quantity = "1",
                        Category = "PHYSICAL_GOODS"
                      },
                      new Item
                      {
                        Name = "Shoes",
                        Description = "Running, Size 10.5",
                        Sku = "sku02",
                        UnitAmount = new Money
                        {
                          CurrencyCode = "USD",
                          Value = "45.00"
                        },
                        Tax = new Money
                        {
                          CurrencyCode = "USD",
                          Value = "5.00"
                        },
                        Quantity = "2",
                        Category = "PHYSICAL_GOODS"
                      }
                    },
                    ShippingDetail = new ShippingDetail
                    {
                      Name = new Name
                      {
                        FullName = "John Doe"
                      },
                      AddressPortable = new AddressPortable
                      {
                        AddressLine1 = "123 Townsend St",
                        AddressLine2 = "Floor 6",
                        AdminArea2 = "San Francisco",
                        AdminArea1 = "CA",
                        PostalCode = "94107",
                        CountryCode = "US"
                      }
                    }
                 }
                }
            };
            return orderRequest;
        }

        public PayPalEnvironment environment()
        {
            return new SandboxEnvironment("AVleUwSDJgxZF_za7_Xwm891Jg3c5ILul7FTm6FCtFjgAfI3q6h0x_uQL8pC0_OCHErnTgeP8DtC3hi8", "ENNlPsuCSXwKCxt70dZA5csc58n9DS_2qhF0sPnsxboje6Tp-wGEoVhw1D3I_jQmd0ydPxqpbo6fb3H2");
        }

        /**
            Returns PayPalHttpClient instance to invoke PayPal APIs.
         */
        public PayPalHttpClient client()
        {
            return new PayPalHttpClient(environment());
        }

        public PayPalHttpClient client(string refreshToken)
        {
            return new PayPalHttpClient(environment(), refreshToken);
        }

        /**
            Use this method to serialize Object to a JSON string.
        */
        public static String ObjectToJSONString(Object serializableObject)
        {
            MemoryStream memoryStream = new MemoryStream();
            var writer = JsonReaderWriterFactory.CreateJsonWriter(
                        memoryStream, Encoding.UTF8, true, true, "  ");
            DataContractJsonSerializer ser = new DataContractJsonSerializer(serializableObject.GetType(), new DataContractJsonSerializerSettings { UseSimpleDictionaryFormat = true });
            ser.WriteObject(writer, serializableObject);
            memoryStream.Position = 0;
            StreamReader sr = new StreamReader(memoryStream);
            return sr.ReadToEnd();
        }

        //private async Task<PayPalPaymentExecutedResponse> ExecutePaypalPaymentAsync(HttpClient http, PayPalAccessToken accessToken, string paymentId, string payerId)
        //{
        //    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, $"v1/payments/payment/{paymentId}/execute");

        //    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken.access_token);

        //    var payment = JObject.FromObject(new
        //    {
        //        payer_id = payerId
        //    });

        //    request.Content = new StringContent(JsonConvert.SerializeObject(payment), Encoding.UTF8, "application/json");

        //    HttpResponseMessage response = await http.SendAsync(request);
        //    string content = await response.Content.ReadAsStringAsync();
        //    PayPalPaymentExecutedResponse executedPayment = JsonConvert.DeserializeObject<PayPalPaymentExecutedResponse>(content);
        //    return executedPayment;
        //}
    }
}
