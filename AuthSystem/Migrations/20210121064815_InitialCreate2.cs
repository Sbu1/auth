﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AssignmentManagement.Migrations
{
    public partial class InitialCreate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Amount",
                table: "AssignmentApplications",
                type: "decimal(10,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "AssignmentApplications",
                type: "nvarchar(10)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "AssignmentApplications");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "AssignmentApplications");
        }
    }
}
