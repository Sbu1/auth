﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AssignmentManagement.PayPalHelper;
using AuthSystem.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;


namespace AuthSystem.Controllers
{
    [Authorize]
    public class AssignmentController : Controller
    {
        private readonly AssignmentContext _context;
        private IWebHostEnvironment _hostingEnvironment;
        private IConfiguration _configuration { get; }
        public AssignmentController(AssignmentContext context, IWebHostEnvironment webHostEnvironment,  IConfiguration configuration)
        {
            _context = context;
            _hostingEnvironment = webHostEnvironment;
            _configuration = configuration;
        }

        // GET: Assignment Applications
        public async Task<IActionResult> Index()
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Email == User.Identity.Name);

            return View(_context.AssignmentApplications.Where(x => x.ApplicationUser.Id == user.Id));
        }

        // GET: Employees/Create
        public IActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new AssignmentApplication());

            return View(_context.AssignmentApplications.Find(id));
        }

        // POST: Employees/Create
        // To protect from overposting attacks, enable the specific properti es you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddOrEdit([Bind("AssignmentId,Institution,Course,Year,DueDate,AdditionalNotes")] AssignmentApplication assignmentApplication, IFormFile postedFile)
        {
            if (ModelState.IsValid)
            {
                string uploads = Path.Combine(_hostingEnvironment.WebRootPath, "Docs");
                string filePath = Path.Combine(uploads, postedFile.FileName);
                using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await postedFile.CopyToAsync(fileStream);
                }

                assignmentApplication.FileName = postedFile.FileName;
                var username = User.Identity.Name;
                assignmentApplication.ApplicationUser = _context.Users.FirstOrDefault(x => x.Email == username);
                if (assignmentApplication.AssignmentId == 0)
                {
                    assignmentApplication.Status = "Submitted";
                    assignmentApplication.Amount = 1000;
                    _context.Add(assignmentApplication);
                }
                else
                {
                    _context.Update(assignmentApplication);
                }

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(assignmentApplication);
        }



        // GET: Employees/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            var employee = await _context.AssignmentApplications.FindAsync(id);
            _context.AssignmentApplications.Remove(employee);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> Checkout(double Amount)
        {
            
            Amount = 1; 
            var payPalApi = new PayPalAPI(_configuration);
            string url = await payPalApi.getRedirectURLToPayPal();
            return Redirect(url);
        }

       
        public async Task<IActionResult> Success()
        {
            //var payPalApi = new PayPalAPI(_configuration);
            //PayPalPaymentExecutedResponse result = await payPalApi.executePayment(paymentId, PlayerID);

            // var user = await _context.Users.FirstOrDefaultAsync(x=>x.Email == User.Identity.Name);
            var application = await _context.AssignmentApplications.FirstOrDefaultAsync(x => x.ApplicationUser.Email == User.Identity.Name); //TODO this will only work if the user has one applications
            application.Status = "Paid";
            application.Amount = 1;
            _context.Update(application);
           
           return RedirectToAction(nameof(Index));
        }
    }
}
